# FastAPI_Facebook-scraper

## Getting started

The goal of this project is to develop an API to get posts and comments from public Facebook pages using the  name of the page, and then store them in a mongodb database.


The API is implemented using fastAPI and the Facebook scraping (posts and comments) is achieved through the facebook_scraper python library.

## Running the project

To build a docker image with the name "FastAPI_fb" for the API, you have to run this command in the same directory as FastAPI.py and Dockerfile .

> docker build -t fastapi_fb .

If you are already in the directory where the Dockerfile is located, put a . instead of the location:
By adding the -t flag, you can tag the new image with a name which will help you when dealing with multiple images

Next, you can run the docker image and test it using:
> docker run -p 8000:8000 fastapi_fb

Now, to start the mongodb database you need to run:

> sudo docker-compose up -d

The up command will pull the mongo image from the docker registry and create the container using the given parameters in the docker-compose.yml file.

> sudo docker exec -it mongodb bash

Using the docker exec command, we can access the terminal of the MongoDB container.
In the bash terminal of the container, we call the mongo command to access MongoDB. 

## API testing 

This request will scrap posts and comments from the page “Forum-des-jeunes-ingenieurs-tunisiens-518661064859157”

http://localhost:8000/scraping/?page=Forum-des-jeunes-ingenieurs-tunisiens-518661064859157

![](tests/scrap_posts.jpg)
![](tests/scrap_comments.jpg)

This request will scrap and save posts and comments in the mongodb database.

http://localhost:8000/scraping/?page=Forum-des-jeunes-ingenieurs-tunisiens-518661064859157&save=True

![](tests/save_fb.jpg)

Show informations from mongodb database

![](tests/show_db.jpg)

