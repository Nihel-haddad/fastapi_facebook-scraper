import pymongo
from pymongo import MongoClient
from fastapi import FastAPI, Request
from facebook_scraper import get_posts
import datetime




app = FastAPI()
client = MongoClient("localhost", 27017)
db = client['fb_posts']

@app.get("/")
async def root():
    return "Welcome"

@app.get("/scraping/")
async def scrap_posts(page: str, limit: int = 5, save: bool=False):
    res = {}
    res["result"] = [post for post in get_posts(page, pages=limit, options={"comments":True})]
    res["time"] = datetime.datetime.now()
    
    if save:
        try: 
            db.fb_posts.insert_many(res["result"])
            return "success"
        except:
            return "error"
    return res["result"]




