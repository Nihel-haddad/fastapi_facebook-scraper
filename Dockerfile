FROM python:3.7

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY FastAPI.py FastAPI.py

CMD [ "uvicorn", "FastAPI:app" , "--reload", "--host=0.0.0.0"]


